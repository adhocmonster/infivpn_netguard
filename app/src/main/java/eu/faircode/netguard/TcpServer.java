package eu.faircode.netguard;

import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketImpl;
import java.net.SocketImplFactory;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by SAN2271 on 2018/6/1.
 */

public class TcpServer {
    public ServerSocketChannel getServerSocketChannel() {
        return serverSocketChannel;
    }

    public void setServerSocketChannel(ServerSocketChannel serverSocketChannel) {
        this.serverSocketChannel = serverSocketChannel;
    }

    public ServerSocketChannel getServerSocketChannel_read() {
        return serverSocketChannel_read;
    }

    public void setServerSocketChannel_read(ServerSocketChannel serverSocketChannel_read) {
        this.serverSocketChannel_read = serverSocketChannel_read;
    }

    public SocketChannel getClientsocketChannel_read() {
        return clientsocketChannel_read;
    }

    public void setClientsocketChannel_read(SocketChannel clientsocketChannel_read) {
        this.clientsocketChannel_read = clientsocketChannel_read;
    }

    public SocketChannel getClientsocketChannel() {
        return clientsocketChannel;
    }

    public void setClientsocketChannel(SocketChannel clientsocketChannel) {
        this.clientsocketChannel = clientsocketChannel;
    }

    //===================================ServerSocketChannel===================================
    ServerSocketChannel serverSocketChannel;
    ServerSocketChannel serverSocketChannel_read;
    SocketChannel clientsocketChannel_read;
    SocketChannel clientsocketChannel;
    private boolean stop=false;
    private ConcurrentLinkedQueue<byte[]> channel_DataQueue=new ConcurrentLinkedQueue<>();
    private Thread recv_Data_Thread=null;
    private int socketCap=100000;
    private int port=44444;
    private int port2=44445;
    private String REMOTE_IP="127.0.0.1";
    private int BUFFER_SIZE=185483;
    public void createChannel(){
        try {
            serverSocketChannel=ServerSocketChannel.open();
            serverSocketChannel_read=ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel_read.configureBlocking(false);
            serverSocketChannel.socket().bind(new InetSocketAddress(REMOTE_IP,port));
            serverSocketChannel_read.socket().bind(new InetSocketAddress(REMOTE_IP,port));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void listenFromClietn() {
        try {
            while (clientsocketChannel == null) {
                clientsocketChannel = serverSocketChannel.accept();
            }
            Log.i("finish","111");
            Log.i("finish","222");
            while (clientsocketChannel_read == null) {
                clientsocketChannel_read = serverSocketChannel_read.accept();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void recvDataFromClient() {
        if(recv_Data_Thread==null) {
            recv_Data_Thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    //read_from_client();
                }
            });
        }
        recv_Data_Thread.start();
    }
}

package eu.faircode.netguard.NIO;

import android.util.Log;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class NIOServer extends NIOServerBase implements Runnable {

    private static final int BUFFER_SIZE = 100000;
    public static boolean connectionFlag=false;
    public static CopyOnWriteArrayList<SocketChannel> clientSocketChannelList = new CopyOnWriteArrayList<>();
    private static InetSocketAddress address = new InetSocketAddress(60610);
    private static ByteBuffer writeBuffer = ByteBuffer.allocate(BUFFER_SIZE);
    private static int written_Length = 0;
    private static byte[] data;
    private static int packet_length = 0;

    public NIOServer() {
        super("TCP");

    }

    @Override
    public void run() {

        try{
            Selector selector = Selector.open();

            ByteBuffer readBuffer = ByteBuffer.allocate(BUFFER_SIZE);

            ServerSocketChannel listening = ServerSocketChannel.open();
            listening.configureBlocking(false);

            try {
                listening.socket().bind(address, 0);
            }catch(BindException e){
                throw new BindException(String.format(super.BIND_FAILED_MESSAGE, protocol, e));
            }catch(java.net.SocketException e){
                throw new BindException(String.format(super.BIND_FAILED_MESSAGE, protocol, e));
            }

            listening.register(selector, SelectionKey.OP_ACCEPT, listening);
            int selectedChannel = -1;

            while (true) {

                if (Thread.currentThread().isInterrupted()){
                    System.out.println(super.THREAD_INTERRUPT_CLEANUP_MESSAGE+" :"+  protocol);;
                    listening.close();
                    clientSocketChannelList.clear();
                    break;
                }

                int count = selector.select();
                if(clientSocketChannelList.size()!=selectedChannel){
                    selectedChannel =  clientSocketChannelList.size();
                    System.out.println("connected a channel: "+ selectedChannel);
                }
                if (count > 0) {
                    Iterator<SelectionKey> it = selector.selectedKeys().iterator();
                    while (it.hasNext()) {

                        SelectionKey key = it.next();
                        it.remove();

                        if (key.isValid() && key.isAcceptable()) {
                            processAcceptable(key);
                        }

                        if (key.isValid() && key.isReadable()) {
                            processReadable(key, readBuffer);
                        }

                        if (key.isValid() && key.isWritable()) {
                            processWritable(key);
                        }
                    }
                }
            }
        }catch(IOException e) {
            System.out.println("problem opening selector");
            e.printStackTrace();
        }


    }


    private static void processWritable(
            SelectionKey key) throws IOException {

        SocketChannel tunnel = (SocketChannel) key.attachment();

        if (writeBuffer.position() > 0) {
            writeBuffer.flip();
            written_Length = tunnel.write(writeBuffer);
            tunnel.register(key.selector(), SelectionKey.OP_READ, tunnel);
            writeBuffer.flip();
            writeBuffer.clear();
        }
    }

    private static void processReadable(
            SelectionKey key,
            ByteBuffer readBuffer) throws IOException {

        readBuffer.clear();
        SocketChannel tunnel = (SocketChannel) key.attachment();

        int r = 0;
        try {
            r = tunnel.read(readBuffer);
        }
        catch(IOException e) {
            key.cancel();
            System.out.println("Connection closed: " + key.channel());
        }

        if (r <= 0) {
            for(SocketChannel sc:clientSocketChannelList){
                if(sc.socket().getInetAddress().getHostAddress().equals(tunnel.socket().getInetAddress().getHostAddress())){
                    clientSocketChannelList.remove(sc);
                }
            }

            tunnel.close();
            key.cancel();
            System.out.println("Connection closed due to EOF");
        } else {
//            String s=new String( readBuffer.array(), Charset.forName("UTF-8") );
//            Log.e(TAG, "length: "+r+" message: "+s.trim());
                readBuffer.flip();
                byte[] data_tmp= new byte[r];
                readBuffer.get(data_tmp, 0, r);
                System.out.println("read length is: "+ r);

                data = processData(data_tmp, tunnel, readBuffer);

                tunnel.register(key.selector(), SelectionKey.OP_WRITE, tunnel);
                readBuffer.flip();

        }
    }


    private static void processAcceptable(
            SelectionKey key) throws IOException {
        System.out.println("processAcceptable");
        SocketChannel tunnel = ((ServerSocketChannel)key.attachment()).accept();

        addToConnectionList(tunnel);

        System.out.println("Accepted " + tunnel.socket());
        tunnel.socket().setTcpNoDelay(true);
        tunnel.configureBlocking(false);

        tunnel.register(key.selector(), SelectionKey.OP_READ, tunnel);

    }


    private static void addToConnectionList(SocketChannel channel){

        if(clientSocketChannelList.isEmpty()){
            clientSocketChannelList.add(channel);
            connectionFlag=false;
        }else{
            for(SocketChannel sc:clientSocketChannelList){
                if(sc.socket().getInetAddress().getHostAddress().equals(channel.socket().getInetAddress().getHostAddress())){
                    //if same IP address then skip
                }else{
                    clientSocketChannelList.add(channel);
                    connectionFlag=false;
                }
            }
        }

    }

    private static byte[] processData(byte[] bytes, SocketChannel clientsocketChannel_read, ByteBuffer buffer) throws IOException{
        buffer.clear();
        int len2=0;
        if(bytes.length<3){
            return bytes;
        }
        if (bytes.length !=(bytes[3]&0xFF)+(bytes[2]&0xFF)*256) {
            while (bytes.length%1348==0) {
                len2 = clientsocketChannel_read.read(buffer);
                if (len2 == -1) {
                    Log.i("in read from ","-1");
                    //    tmp.clear();
                    continue;
                }
                buffer.flip();
                byte[] bytesTmp = new byte[len2];
                buffer.get(bytesTmp, 0, len2);
                buffer.clear();
                byte[] bytessum = new byte[bytes.length + bytesTmp.length];
                System.arraycopy(bytes, 0, bytessum, 0, bytes.length);
                System.arraycopy(bytesTmp, 0, bytessum, bytes.length, bytesTmp.length);
                bytes = bytessum;
                packet_length = bytes.length;
                if (40 <= len2 && len2 < 1348) {
                    return bytes;
                }
            }
        }
        return bytes;

    }

    public static int writeToClient(byte[] message){
        if(message.length>0){
            if(message.length<writeBuffer.capacity()) {
                written_Length = 0;
                writeBuffer.put(message);
            }else {
                written_Length = -1;
               Log.e("Write To Client", "message too big, handle this");
            }

        }

        while(written_Length == 0){

        }

        return written_Length;

    }

    public static byte[] getData(){
        data = new byte[0];
        while(data.length==0){

        }
        return data;
    }

}

package eu.faircode.netguard.NIO;

import java.net.InetSocketAddress;

public class NIOServerBase {


    /**
     * Message to describe starting of port forwarding thread.
     */
    public static final String START_MESSAGE = "%s Port Forwarding Started from port %s to port %s";

    /**
     * Message to describe a failed binding from a Forwarding class.
     */
    public static final String BIND_FAILED_MESSAGE = "Could not bind port %s for %s Rule '%s'";

    /**
     * Message to describe a thread interruption.
     */
    public static final String THREAD_INTERRUPT_CLEANUP_MESSAGE = "%s Thread interrupted, will perform cleanup";


    /**
     * The name of the protocol being used to forward.
     */
    protected final String protocol;


    public NIOServerBase(String protocol){
        this.protocol = protocol;
    }
}

package eu.faircode.netguard.NIO;

import android.util.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import eu.faircode.netguard.ServiceSinkhole;

import static java.lang.Thread.sleep;

public class NIOClient implements Runnable{


    public  SocketChannel socketChannel;
    String REMOTE_IP = "192.168.49.1";
    int port = 60610;
    String message = "hello";
    private static final int BUFFER_SIZE = 100000;
    public static boolean start = false;

    private ByteBuffer readBuffer = ByteBuffer.allocate(BUFFER_SIZE);
    private static ByteBuffer writeBuffer;
    private ServiceSinkhole serviceSinkhole;
    private LinkedBlockingQueue<byte[]> DataQueue = new LinkedBlockingQueue<byte[]>();


    @Override
    public void run() {

//
//        while(!start){
//
//        }

        try {
            serviceSinkhole = new ServiceSinkhole();
            Selector selector = Selector.open();

            socketChannel = SocketChannel.open();

            socketChannel.configureBlocking(false);
            socketChannel.register(selector, SelectionKey.OP_CONNECT);
            socketChannel.connect(new InetSocketAddress(REMOTE_IP, port));

            int selectedChannel = -1;

            while (true) {

                if (Thread.currentThread().isInterrupted()){
                    socketChannel.close();
                    writeBuffer.clear();
                    break;
                }

                int count = selector.select();
                if(count!=selectedChannel){
                    selectedChannel =  count;
                    System.out.println("selected a channel: "+selectedChannel);
                }
                if (count > 0) {
                    Iterator<SelectionKey> it = selector.selectedKeys().iterator();
                    while (it.hasNext()) {

                        SelectionKey key = it.next();
                        it.remove();

                        if (key.isValid() && key.isConnectable()) {
                            processConnectable(key);
                        }

                        if (key.isValid() && key.isReadable()) {
                            processReadable(key);
                        }

                        if (key.isValid() && key.isWritable()) {
                            processWritable(key);
                        }

                        mergePackage();
                    }
                }
            }




        } catch (IOException e) {
            e.printStackTrace();
        }


    }



    public void processConnectable(SelectionKey key){
        try {
                socketChannel.finishConnect();
                socketChannel.socket().setTcpNoDelay(true);
                socketChannel.socket().setKeepAlive(true);
                socketChannel.register(key.selector(), SelectionKey.OP_WRITE);

        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void processWritable(SelectionKey key){

        tunToSocket();

        try {

            if(writeBuffer != null) {
//                writeBuffer.flip();
                socketChannel.write(writeBuffer);
                writeBuffer.flip();
                writeBuffer.clear();
                socketChannel.register(key.selector(), SelectionKey.OP_READ);
                writeBuffer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void processReadable(SelectionKey key){

        readBuffer.clear();
        int r = 0;
        try {
            r = socketChannel.read(readBuffer);


            if (r <= 0) {
                socketChannel.close();
                key.cancel();
                System.out.println("Connection closed due to EOF");
            }else {

                readBuffer.flip();
                byte[] data= new byte[r];
                readBuffer.get(data, 0, r);
                System.out.println(r);
                DataQueue.offer(data);

                socketChannel.register(key.selector(), SelectionKey.OP_WRITE);
                readBuffer.flip();

            }

            Thread.sleep(500);


        }
        catch(IOException e) {
            key.cancel();
            System.out.println("Connection closed: " + key.channel());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void tunToSocket(){

//        writeBuffer = serviceSinkhole.readFromTun();

    }


    private void mergePackage() {
        while(DataQueue.size()!=0) {
            byte[] bytes = DataQueue.poll();
            // Log.i("bytes length",":"+bytes.length);
            if(bytes.length!=(bytes[3]&0xFF)+(bytes[2]&0xFF)*256){
                while (true) {
                    byte[] bytestemp = new byte[0];
                    try {

                        byte[] peek=DataQueue.peek();
                        if(peek==null) {
                            sleep(10);
                            continue;
                        }

                        if (peek[0] == 69 && peek[1] == 0) {
                            break;
                        }

                        bytestemp = DataQueue.poll(100, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    byte[] bytessum = new byte[bytes.length + bytestemp.length];
                    System.arraycopy(bytes, 0, bytessum, 0, bytes.length);
                    System.arraycopy(bytestemp, 0, bytessum, bytes.length, bytestemp.length);
                    bytes = bytessum;
                    Log.i("merge bytes length", "" + bytes.length);
                }
            }
            int pos=3;
            int passlen=0;
//            if(bytes.length!=1348&&bytes.length>1348&&bytes[0]!=69){
//                Log.i("bytes length",":"+bytes.length);
//
//            }
            if(bytes.length>0) {
                cutpackage(bytes);
            }
        }

    }


    //cut package in Ipv4
    private void cutpackage(byte[] buffer) {
        //       Log.i("buffer length",""+(buffer.length));
        if(buffer.length>65483) {
            Log.i("==cutpacket>65483==", "cutpacket" + buffer.length);
        }
        int pos=3;
        int passlen=(buffer[pos]&0xFF)+256*(buffer[pos-1]&0xFF);
//        Log.i("passlen-----",":"+passlen);
        while(passlen<=buffer.length){
            //  Log.i("====== cutpacket=====","cutpacket");
            if(pos<0||(buffer[pos]&0xFF)<0){
                Log.i("pos<0",pos+":");
                return;
            }
            int length=(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256;
            if(length<0){
                return;
            }
            byte[] tempbytes=new byte[length];
//            if((buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256+passlen>buffer.length||buffer[pos-3]!=69){
//                Log.i("not 69","buffer length"+buffer.length+"temp length:"+((buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256+passlen));
//                for(int i=0;i<5000;i++) {
//                    Log.i("bytes",i+":"+(buffer[i]&0xFF));
//                }
//                return;
//            }
            if((buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256+pos-3>buffer.length){
                return;
            }
            System.arraycopy(buffer,pos-3,tempbytes,0,(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256);
//            for(int i=0;i<tempbytes.length;i++) {
//                    Log.i("tempbytes",i+":"+(tempbytes[i]&0xFF));
//            }
//            if(buffer.length>=65483){
////            Log.i("tempbytes length",""+(tempbytes.length));
//            }
            //Log.i("in cutpakage","===");
            //               Log.i("write to tun length",":"+tempbytes.length);
            Log.i("cut","before");
            serviceSinkhole.writeToTun(tempbytes,tempbytes.length);
            Log.i("cut","after");

            //Log.i("finish","write===");
//            Log.i("passlen",""+passlen+" "+pos);
            pos=pos+(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256;
            if(pos>buffer.length){
//                for(int i=0;i<tempbytes.length;i++){
//                    Log.i("tempbytes",i+":"+tempbytes[i]);
//                }
                return;
            }
            passlen+=(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256;
//            if(buffer.length>=65483) {
////                Log.i("passlen", "" + passlen + " " + pos);
//            }
            if(buffer[pos]==0&&buffer[pos-1]==0){
                return;
            }
        }
    }
}

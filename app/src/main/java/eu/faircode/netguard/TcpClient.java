package eu.faircode.netguard;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

/**
     * Created by SAN2271 on 2018/5/29.
 */

public class TcpClient {
    private Thread TuntoSo_Thread;
    private Thread SotoTun_Thread;
    private Thread createsocket;
    private InputStream inputStream;
    private InputStream testInputStream;
    private OutputStream outputStream, outputStreamflag;
    private boolean stop_handle=false;
    private static final int BUFFER_SIZE = 185483;
    public Socket socket;
    public Socket clientToserver;
    public ServiceSinkhole serviceSinkhole;
    public boolean stop=false;
    public Socket createsocket() {
        Log.i("java","in create client");
            serviceSinkhole=new ServiceSinkhole();
            createsocket=new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.i("int createClient","before create Client");
                        socket = new Socket("127.0.0.1", 44444);
                        inputStream = socket.getInputStream();
                        outputStreamflag=socket.getOutputStream();
                        clientToserver=new Socket("127.0.0.1",44445);
                        outputStream = clientToserver.getOutputStream();
                        testInputStream=clientToserver.getInputStream();



                        connectServerWithTCPSocket();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            createsocket.start();
        return socket;
    }
    public void connectServerWithTCPSocket() {

          if(TuntoSo_Thread==null) {
              TuntoSo_Thread=new Thread(new Runnable() {
                  @Override
                  public void run() {
                      while(!stop) {
                          TunToSocket();
                      }
                  }
              });

          }
          if(SotoTun_Thread==null) {
              SotoTun_Thread=new Thread(new Runnable() {
                  @Override
                  public void run() {
                      while(!stop) {
                          SocketToTun();
                      }
                  }
              });

          }
        TuntoSo_Thread.start();
        SotoTun_Thread.start();

    }
    public int SocketToTun() {
        Log.i("in socket to tun","ccccc");

        byte[] bytes=new byte[BUFFER_SIZE];

        int len=0;
        try {
            if(socket!=null) {
               // String s;
                len = inputStream.read(bytes);
               //BufferedReader in=new BufferedReader(new InputStreamReader(inputStream));
//              if((s =in.readLine())!=null){
//                  Log.i("s",s);
//                  bytes=s.getBytes();
//                  for(int i=0;i<len;i++){
//                      Log.i("bytes",i+":"+bytes[i]);
//                  }
//                  len=bytes.length;
//              }
                Log.i("length=",String.valueOf(len));
                if (len > 0) {
                   int res=serviceSinkhole.writeToTun(bytes,len);
                   //outputStreamflag.write(res);
                }
            }
            return 1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void TunToSocket() {
        //Log.i("java","in tun to socket");
        byte[] bytes ;
        bytes = serviceSinkhole.readFromTun();
        if (bytes != null) {
//            for (int i = 0; i < bytes.length; i++) {
//                Log.i("bytes", i + ":" + bytes[i]);
//            }

            byte[] buffer=new byte[bytes.length];
            System.arraycopy(bytes, 0, buffer, 0, bytes.length);
            //buffer[4999]= (byte) bytes.length;
            int len = buffer.length;
            try {
                if (socket != null) {
                    //Log.i("java",bytes.toString());
                    if (len > 0) {
                        outputStream.write(buffer);
                    }
                }
            } catch (IOException e) {
                Log.i("tun to socket write","worry");
                e.printStackTrace();
            }
        }
    }

    //============================Use_Selector_in_Client===============================
    /**
     *
     * Created by ascend on 2017/6/13 10:36.
     */
    //通道管理器
    private Selector selector;
    private final static String REMOTE_IP = "192.168.49.1";
    //private final static String REMOTE_IP = "127.0.0.1";
    private int port=44444;
    private Thread clientstart;

    public void runClient(){
        if(clientstart==null) {

            clientstart=new Thread(new Runnable() {
                @Override
                public void run() {
                    serviceSinkhole=new ServiceSinkhole();
                    TcpClient client = new TcpClient();
                    try {
                        client.initClient(REMOTE_IP, port);
                        client.connect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            clientstart.start();

        }
    }

    /**
     * 获得一个Socket通道，并对该通道做一些初始化的工作
     * @param ip 连接的服务器的ip
     * @param port  连接的服务器的端口号
     * @throws IOException
     */
    public void initClient(String ip,int port) throws IOException {


        // 获得一个Socket通道
        SocketChannel channel = SocketChannel.open();
        // 设置通道为非阻塞
        channel.configureBlocking(false);
        // 获得一个通道管理器
        this.selector = Selector.open();

        // 客户端连接服务器,其实方法执行并没有实现连接，需要在listen（）方法中调
        //用channel.finishConnect();才能完成连接
        channel.connect(new InetSocketAddress(ip,port));
        //将通道管理器和该通道绑定，并为该通道注册SelectionKey.OP_CONNECT事件。
        channel.register(selector, SelectionKey.OP_CONNECT);
    }

    /**
     * 采用轮询的方式监听selector上是否有需要处理的事件，如果有，则进行处理
     * @throws IOException
     */
    public void connect() throws IOException {
        // 轮询访问selector
        while (true) {
            // 选择一组可以进行I/O操作的事件，放在selector中,客户端的该方法不会阻塞，
            //这里和服务端的方法不一样，查看api注释可以知道，当至少一个通道被选中时，
            //selector的wakeup方法被调用，方法返回，而对于客户端来说，通道一直是被选中的
            selector.select();
            // 获得selector中选中的项的迭代器
            Iterator<SelectionKey> ite = this.selector.selectedKeys().iterator();
            while (ite.hasNext()) {
                SelectionKey key = (SelectionKey) ite.next();
                // 删除已选的key,以防重复处理
                ite.remove();
                // 连接事件发生
                if (key.isConnectable()) {
                    SocketChannel channel = (SocketChannel) key.channel();
                    // 如果正在连接，则完成连接
                    if(channel.isConnectionPending()){
                        channel.finishConnect();
                    }
                    // 设置成非阻塞
                    channel.configureBlocking(false);
                    //在这里可以给服务端发送信息哦
//                    channel.write(ByteBuffer.wrap(new String("向服务端发送了一条信息").getBytes("utf-8")));
                    //在和服务端连接成功之后，为了可以接收到服务端的信息，需要给通道设置读的权限。
                    channel.register(this.selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);                                            // 获得了可读的事件
                } else if (key.isReadable()) {
                    socket_to_Tun(key);
                }else if(key.isWritable()){
                    tun_to_Socket(key);
                }
            }
        }
    }
    /**
     * 处理读取服务端发来的信息 的事件
     * @param key
     * @throws IOException
     */
    public void socket_to_Tun(SelectionKey key) throws IOException{
        Log.i("in socket to tun","bbbb");
        //和服务端的read方法一样
        // 服务器可读取消息:得到事件发生的Socket通道
        SocketChannel channel = (SocketChannel) key.channel();
        // 创建读取的缓冲区
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        int len=channel.read(buffer);
        byte[] data = buffer.array();
        serviceSinkhole.writeToTun(data,len);


    }
    public void tun_to_Socket(SelectionKey key) throws IOException {
        SocketChannel channel=(SocketChannel)key.channel();
        byte[] bytes=serviceSinkhole.readFromTun();
        //byte[] buffertemp=new byte[bytes.length];
        //System.arraycopy(bytes, 0, buffertemp, 0, bytes.length);
        ByteBuffer buf = ByteBuffer.wrap(bytes, 0, bytes.length);
        channel.write(buf);
    }



    //============================Use_SocketChannel_in_Client===============================
    private SocketChannel socketChannel;
    private SocketChannel socketChannel_write;
    private Socket socketaaa;
    private Thread queueToTun;
    private int socketCap=100000;
    private int port2=44445;
    byte[] bytes;
    private BlockingQueue<byte[]> DataQueue=new LinkedBlockingQueue<byte[]>();
    private ByteBuffer byteBuffer=ByteBuffer.allocate(BUFFER_SIZE);
    public void createclient(){
        clientstart=new Thread(new Runnable() {

            @Override
            public void run() {
                try {

                    socketChannel=SocketChannel.open();
                    socketChannel_write=SocketChannel.open();
                    socketChannel_write.socket().setSendBufferSize(5000);
                    socketChannel.connect(new InetSocketAddress(REMOTE_IP,port));
                    socketChannel_write.connect(new InetSocketAddress(REMOTE_IP,port2));
                    socketChannel.configureBlocking(false);
                    socketChannel_write.configureBlocking(false);
                    //socketChannel.connect(new InetSocketAddress(REMOTE_IP,port));
                    //socketChannel_write.connect(new InetSocketAddress(REMOTE_IP,port2));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
//        try {
//            socketChannel=SocketChannel.open();
//            socketChannel.configureBlocking(false);
//            while(true) {
//                if(socketChannel.connect(new InetSocketAddress(REMOTE_IP,port))){
//                    break;
//                }
//                sleep(100);
//            }
//
//
//        } catch (IOException | InterruptedException e) {
//            e.printStackTrace();
//        }
        clientstart.start();
        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        runclient();
    }

    public void runclient() {
        serviceSinkhole=new ServiceSinkhole();
        //serviceSinkhole.startClient();
        if(TuntoSo_Thread==null) {
            TuntoSo_Thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    while(!stop) {
                        tunToSocket();
                    }
                }
            });

        }
        if(SotoTun_Thread==null) {
            SotoTun_Thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    while(!stop) {
                        readfromsserver();
                    }
                }
            });

        }
        if(queueToTun==null) {
            queueToTun=new Thread(new Runnable() {
                @Override
                public void run() {
                    while(!stop){
                        if(DataQueue.size()!=0) {
                            mergePackage();
                        }
                    }
                }
            });
        }
        TuntoSo_Thread.start();
        SotoTun_Thread.start();
        queueToTun.start();
    }
    public void socketToTun() {
        //Log.i("in socket to tun","aaaaa");
     //   ByteBuffer byteBuffer=ByteBuffer.allocate(BUFFER_SIZE);
        try {
            int len= socketChannel.read(byteBuffer);

            if(len>0) {

                byte[] bytes = new byte[len];
                byteBuffer.flip();
                byteBuffer.get(bytes, 0, len);
                byteBuffer.clear();
//                for(int i=0;i<bytes.length;i++){
//                    Log.i("bytes===",i+":"+bytes[i]);
//                }
                //if(bytes.length>30000)
                 Log.i("size",""+bytes.length);
                if(len>40){
                    for(int i=0;i<len;i++){
                        Log.i("bytes",i+":"+(bytes[i]&0xFF));
                    }
                }
                int bytelen = (bytes[2]&0xFF)*256+(bytes[3]&0xFF);
               // Log.i("byte==length",""+bytes[3]+","+bytes[2]+"::"+bytelen+":"+bytes.length);

                if(bytes.length<(bytes[3]&0xFF)+(bytes[2]&0xFF)*256){
                    mergePackage();
                }
               if(bytelen!=bytes.length&&bytes[0]==69) {
                   if(len==65483) {
                       ByteBuffer tempbuffer=ByteBuffer.allocate(BUFFER_SIZE);
                       Log.i("in big buffersize====",""+len);
                       int len2=0;
                       for(int i=0;i<200;i++){
                       len2=socketChannel.read(tempbuffer);

                       if(len2>0){
                           Log.i("len2",""+len2);
                           byte[] tempbytes=new byte[len2];
                           tempbuffer.flip();
                           tempbuffer.get(tempbytes,0,len2);
//                           for(int j=0;j<tempbytes.length;j++){
//                               Log.i("tempbytes",j+":"+tempbytes[j]);
//                           }
                           byte[] bytessum=new byte[len2+len];
                           System.arraycopy(bytes,0,bytessum,0,bytes.length);
                           System.arraycopy(tempbytes,0,bytessum,bytes.length,tempbytes.length);
                           bytes=bytessum;
                           break;
                       }

                       }


                   }
                    cutpackage(bytes);
                    return;
                }else if(bytelen<0) {
                   return;
                }else{
                      // Log.i("no cut length====",""+len);
                   serviceSinkhole.writeToTun(bytes, len);
               }
                //byte[] tempbytes=new byte[bytes.length-1];
                //System.arraycopy(bytes,1,tempbytes,0,bytes.length-1);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //cut package in Ipv4
    private void cutpackage(byte[] buffer) {
 //       Log.i("buffer length",""+(buffer.length));
        if(buffer.length>65483) {
            Log.i("==cutpacket>65483==", "cutpacket" + buffer.length);
        }
        int pos=3;
        int passlen=(buffer[pos]&0xFF)+256*(buffer[pos-1]&0xFF);
//        Log.i("passlen-----",":"+passlen);
        while(passlen<=buffer.length){
          //  Log.i("====== cutpacket=====","cutpacket");
            if(pos<0||(buffer[pos]&0xFF)<0){
                Log.i("pos<0",pos+":");
                return;
            }
            int length=(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256;
            if(length<0){
                return;
            }
            byte[] tempbytes=new byte[length];
//            if((buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256+passlen>buffer.length||buffer[pos-3]!=69){
//                Log.i("not 69","buffer length"+buffer.length+"temp length:"+((buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256+passlen));
//                for(int i=0;i<5000;i++) {
//                    Log.i("bytes",i+":"+(buffer[i]&0xFF));
//                }
//                return;
//            }
            if((buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256+pos-3>buffer.length){
                return;
            }
            System.arraycopy(buffer,pos-3,tempbytes,0,(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256);
//            for(int i=0;i<tempbytes.length;i++) {
//                    Log.i("tempbytes",i+":"+(tempbytes[i]&0xFF));
//            }
//            if(buffer.length>=65483){
////            Log.i("tempbytes length",""+(tempbytes.length));
//            }
            //Log.i("in cutpakage","===");
 //               Log.i("write to tun length",":"+tempbytes.length);
                Log.i("cut","before");
            serviceSinkhole.writeToTun(tempbytes,tempbytes.length);
            Log.i("cut","after");

            //Log.i("finish","write===");
//            Log.i("passlen",""+passlen+" "+pos);
            pos=pos+(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256;
            if(pos>buffer.length){
//                for(int i=0;i<tempbytes.length;i++){
//                    Log.i("tempbytes",i+":"+tempbytes[i]);
//                }
                return;
            }
            passlen+=(buffer[pos]&0xFF)+(buffer[pos-1]&0xFF)*256;
//            if(buffer.length>=65483) {
////                Log.i("passlen", "" + passlen + " " + pos);
//            }
            if(buffer[pos]==0&&buffer[pos-1]==0){
                return;
            }
        }
    }

    private void mergePackage() {
        while(DataQueue.size()!=0) {
            byte[] bytes = DataQueue.poll();
            // Log.i("bytes length",":"+bytes.length);
            if(bytes.length!=(bytes[3]&0xFF)+(bytes[2]&0xFF)*256){
            while (true) {
                byte[] bytestemp = new byte[0];
                try {

                    byte[] peek=DataQueue.peek();
                    if(peek==null) {
                        sleep(10);
                        continue;
                    }

                        if (peek[0] == 69 && peek[1] == 0) {
                            break;
                        }

                    bytestemp = DataQueue.poll(100, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                byte[] bytessum = new byte[bytes.length + bytestemp.length];
                System.arraycopy(bytes, 0, bytessum, 0, bytes.length);
                System.arraycopy(bytestemp, 0, bytessum, bytes.length, bytestemp.length);
                bytes = bytessum;
                Log.i("merge bytes length", "" + bytes.length);
            }
        }
                int pos=3;
                int passlen=0;
//            if(bytes.length!=1348&&bytes.length>1348&&bytes[0]!=69){
//                Log.i("bytes length",":"+bytes.length);
//
//            }
                if(bytes.length>0) {
                    cutpackage(bytes);
                }
        }

    }
    private void mergePackge2(){
        while(DataQueue.size()!=0){
            byte[] bytes=DataQueue.poll();
        }
    }
    private void readfromsserver() {
    //    ByteBuffer byteBuffer=ByteBuffer.allocate(BUFFER_SIZE);
        int len= 0;
        try {
            len = socketChannel.read(byteBuffer);
            if(len>0) {

                byte[] bytes = new byte[len];
                byteBuffer.flip();
                byteBuffer.get(bytes, 0, len);
                    DataQueue.offer(bytes);
                byteBuffer.clear();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void clientWriteToTun(){

    }

    public void tunToSocket() {
        bytes=serviceSinkhole.readFromTun();
//        if(bytes.length>0) {
//            Log.i("read from tun", "" + bytes.length);
//        }
//        for(int i=0;i<bytes.length;i++){
//            Log.i("bytes",i+":"+bytes[i]);
//        }
        if(bytes!=null){
            ByteBuffer byteBuffer=ByteBuffer.wrap(bytes);
            try {
                if(bytes.length>0) {
                    socketChannel_write.write(byteBuffer);
                }


            } catch (IOException e ) {
                e.printStackTrace();
            }
        }

    }
}
